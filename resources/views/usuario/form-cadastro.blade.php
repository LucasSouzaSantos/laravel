@extends('layout')

@section('titulo')
    formulario
@endsection
@section('cabecalho')
    <p>form</p>
@endsection
@section('pt-principal')
    <div class="container">
        <div class="row">   
            <div class="col-12">
            <form class="needs-validation" novalidate method="post">
            @csrf
            <div class="form-row">
              <div class="col-md-4 mb-3">
                <label for="validationCustom01">Nome</label>
                <input type="text" class="form-control" name="nome" id="validationCustom01" placeholder="First name" value="Mark" required>
                <div class="valid-feedback">
                  Looks good!
                </div>
              </div>
            <button class="btn btn-primary" type="submit">Submit form</button>
          </form>
          
          
            </div>
        </div>
    </div>
@endsection
@section('rodape')
@endsection